<section class="fslider">
		<div class="slider">
			<ul class="slides">
		    <li>
		    	<img src="images/banner3.jpg">
		    	<div class="caption center-align black-text">  
		        <h3 style="font-size: 3rem !important; font-style: bold !important; font-family: 'Bree Serif', serif;">Take away bakery</h3>  
		        <h5 class="light black-text text-lighten-3"><strong>Món quà ngọt ngào!</strong></h5>  
		      </div>  
		    </li>

		    <li>
		    	<img src="images/banner4.jpg">
		    	<div class="caption center-align black-text">  
		        <h3 style="font-size: 3rem !important; font-style: bold !important; font-family: 'Bree Serif', serif;">Cảm hứng ứng dụng !</h3>  
		        <h5 class="light black-text text-lighten-3"><strong>Nhiều năm kinh nghiệm!</strong></h5>  
		      </div>  
		    </li>
		    </ul>
		   
	  </div>
	</section>