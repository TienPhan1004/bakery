<section class="fabout">
		<div class="section white center">
		      <div class="row container">
		        <h2 class="header">THÔNG TIN</h2>
		        <p class="grey-text text-darken-3 lighten-3">Take away bakery - là một thương hiệu mới có mặt ở tại Nha Trang năm 2020. Hiện tại, Take away bakery có trên 200 loại bánh phục vụ thực khách mỗi ngày. Riêng về kem thì Bakery có sự đa dạng với nhiều vị kem khác nhau, từ kem truyền thống đến các loại tân thời mang hơi hướng hiện đại. Dù hương vị nào thì kem nơi đây cũng có hương vị đặc trưng ăn là nhớ mãi. 
Không những món ngon mà không gian nơi đây cũng là một điểm nhấn để thu hút khách hàng. Vừa cổ kính, vừa ấm áp và còn rất ngọt ngào lại không kém phần sang trọng là những gì bạn cảm nhận được khi bước vào không gian của quán.
               </div>
	</section>