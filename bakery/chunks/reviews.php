<section class="freviews">

		<div class="section white center">
			<h3 class="header">ĐIỀU CHÚNG TÔI MANG LẠI</h3>
			<div class="carousel myreviews" style="margin-bottom: 35px;">
			    <a class="carousel-item" href="#one!">
			    	<div class="row">
					    <div class="col s12">
					      <div class="card-panel teal" style="background: #ee6e73 !important;">
					        <span class="white-text">"Cái tên chẳng còn gì xa lạ với bao người, sở hữu thiết kế đậm chất bánh bèo lại còn có view nhìn ra phố đi bộ rất đẹp nữa nên dễ dàng khiến các cô nàng phát cuồng"<br>-<br> <strong>Không gian </strong>
					        </span>
					      </div>
					    </div>
					  </div>
			    </a>
			    <a class="carousel-item" href="#two!">
			    	<div class="row">
					    <div class="col s12">
					      <div class="card-panel teal" style="background: #ee6e73 !important;">
					        <span class="white-text">"Bánh phù hợp với khẩu vị, kem bánh làm ko ngọt ăn không ngán. Bánh size nhỏ thì tầm từ 15k-35k xinh xinh,còn bánh size lớn bạn có thể ăn thử thích vị nào thì mua vị đó"<br>-<br> <strong>Nhu cầu</strong>
					        </span>
					      </div>
					    </div>
					  </div>
			    </a>
			    <a class="carousel-item" href="#three!">
			    	<div class="row">
					    <div class="col s12">
					      <div class="card-panel teal" style="background: #ee6e73 !important;">
					        <span class="white-text">"Không thể cưỡng vì bánh vừa nóng vừa giòn còn thơm mùi quế hoặc bánh Peach Cheesecake ăn bùi bùi"<br>-<br> <strong>Apple Pie</strong>
					        </span>
					      </div>
					    </div>
					  </div>
			    </a>
			    <a class="carousel-item" href="#four!">
			    	<div class="row">
					    <div class="col s12">
					      <div class="card-panel teal" style="background: #ee6e73 !important;">
					        <span class="white-text">"Bạn sẽ được trải nghiệm cách ăn bánh hoàn toàn khác biệt, mua vé buffet ăn bánh thả ga nhé. Bánh lên liên tục và nhiều loại, tha hồ thưởng thức các loại bánh khác nhau"<br>-<br> <strong>Trải nghiệm</strong>
					        </span>
					      </div>
					    </div>
					  </div>
			    </a>
			    <a class="carousel-item" href="#five!">
			    	<div class="row">
					    <div class="col s12">
					      <div class="card-panel teal" style="background: #ee6e73 !important;">
					        <span class="white-text">"Team hảo ngọt thường xuyên lui tới vì kết vị ngọt bùi. Mỗi mùa đều mang lại sự bất ngờ cho ra mắt nhiều loại bánh độc đáo hấp dẫn"<br>-<br> <strong>Mùa bakery</strong>
					        </span>
					      </div>
					    </div>
					  </div>
			    </a>
			    <a class="carousel-item" href="#six!">
			    	<div class="row">
					    <div class="col s12">
					      <div class="card-panel teal" style="background: #ee6e73 !important;">
					        <span class="white-text">"Nhiều bạn nhớ đến Bakery đầu tiên, chất lượng bánh của Bakery khỏi bàn cãi nhiều rồi. Không gian quán nhỏ nhỏ xinh xinh có view ngồi ngắm đường phố bao đẹp"<br>-<br> <strong>Cupcake</strong>
					        </span>
					      </div>
					    </div>
					  </div>
			    </a>
			    <a class="carousel-item" href="#seven!">
			    	<div class="row">
					    <div class="col s12">
					      <div class="card-panel teal" style="background: #ee6e73 !important;">
					        <span class="white-text">" Take away bakery khi ra mắt tại Nha Trang nhanh chóng chiếm được cảm tình của các bạn trẻ yêu thích bánh ngọt. Hiện tại Le Castella đã có 2 chi nhánh tại Nha Trang"<br>-<br> <strong>Ra Mắt</strong>
					        </span>
					      </div>
					    </div>
					  </div>
			    </a>
			  </div>
		</div>
	</section>